import click
from app import app


# app.config.from_object('app.config.DevConfig')
app.config.from_object('app.config.ProdConfig')

@click.command()
@click.option("--host", default="192.168.1.128", help="Host IP address")
@click.option("--port", default=8080, help="Port number")
@click.option("--debug", is_flag=True, default=app.config['DEBUG'])
def run(host, port, debug):
	app.run(
		host=host,
		port=port,
		ssl_context=('cert.pem', 'key.pem'), 
		debug=debug)

if __name__ == '__main__':
	run()

