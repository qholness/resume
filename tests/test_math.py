import unittest
from hypothesis import given
from hypothesis import strategies as st
import sys
sys.path.append("..")
from app.services.math_helpers import euclidean_distance


class TestMath(unittest.TestCase):
	@given(
		x1=st.floats(allow_infinity=False),
		y1=st.floats(allow_infinity=False),
		x2=st.floats(allow_infinity=False),
		y2=st.floats(allow_infinity=False)
	)
	def test_00(self, x1, y1, x2, y2):
		euclidean_distance(x1, y1, x2, y2)


if __name__ == '__main__':
	unittest.main()