def euclidean_distance(x1, y1, x2, y2):
	xdif = x2 - x1
	ydif = y2 - y1
	try:
		xdif_squared = xdif ** 2
		ydif_squared = ydif ** 2
	except OverflowError:
		return False
	total = xdif_squared + ydif_squared
	rooted = total ** .5
	return rooted