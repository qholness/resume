from flask import Flask

app = Flask(__name__)

from app.views import base_views, map_views, path_views
