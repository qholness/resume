from app import app
from app.services.path_helper import Node
from flask import render_template, session
from random import randint

@app.route("/path-review")
def path_review():
    rand_path = lambda: [Node(x=randint(0,1000), y=randint(0,1000)) for _ in range(50)]
    if session.get("path"):
        path = session['path']
        coords = [i for i in path.values()]
        if len(coords) > 1:
            path = [Node(x=_['lat'], y=180 - abs(_['lon'])) for _ in coords]
        else:
            path = rand_path()
    else:
        path = rand_path()
    for node in path:
        print(node.x, node.y)
    xs = [node.x for node in path]
    ys = [node.y for node in path]
    min_x, max_x = min(xs), max(xs)
    min_y, max_y = min(ys), max(ys)
    return render_template("path-review.html", path=path,
        min_x=min_x, max_x=max_x,
        min_y=min_y, max_y=max_y)
