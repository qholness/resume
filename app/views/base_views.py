from app import app
from app.services.path_helper import Node
from flask import render_template, session
from random import randint

@app.route("/info")
def info():
    return render_template("info.html")
