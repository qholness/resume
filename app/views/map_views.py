import schedule
import pickle
from app import app
from app.services.math_helpers import euclidean_distance as edist
from app.services.path_helper import Node
from flask import render_template, request, session, jsonify, url_for
from random import choice, uniform

global geolocs
geolocs = {}
home_base = {}

def reset_geolocs():
	global geolocs
	# with open("data{}.pickle".format(str(datetime.now())), "wb+") as data:
	# 	pickle.dump(geolocs, data)
	# geolocs = {}

schedule.every().sunday.do(reset_geolocs)
def run_scheduled(func):
	global geolocs
	def decor(*args, **kwargs):
		return func(*args, **kwargs)
	from datetime import datetime
	schedule.run_pending()
	return decor

@run_scheduled
@app.route("/")
@app.route("/my-map")
def my_map():
	return render_template("my-map.html")

@run_scheduled
@app.route("/home-base")
def homebase():
	lat = home_base.get('lat', '0')
	lon = home_base.get('lon', '0')
	return ",".join(map(str, [lat, lon]))


@run_scheduled
@app.route("/coords", methods=["POST"])
def coords():
	global geolocs
	
	# Forcing a fine limit of 10000 entries for sanity's sake.
	if len(geolocs) > 10000:
		return "Max users reached.", ""

	if request.data:
		try:
			keys = ['lat', 'lon', 'char', 'tag', 'online']
			vals = request.data.split(",")
			data = dict(zip(keys, vals))
			data['lat'], data['lon'] = map(float, (data['lat'], data['lon']))
			data['char'], data['tag'], data['online'] = \
				map(str, (data['char'], data['tag'], data['online']))

			if len(geolocs) == 0:
				set_home_base({'lat' : data['lat'], 'lon' : data['lon']})

			geolocs[request.remote_addr] = data
			session['path'] = geolocs.copy()
		except KeyError:
			pass
	def get_online_warriors(adict):
		count = 0
		for ip, payload in adict.items():
			if payload.get("online") in [True, "true"]:
				count += 1
		return count
	# ret_string = "<br>Online: {}<br>Total: {}".format(
	# 	str(get_online_warriors(geolocs)), str(len(geolocs)))
	ret_string = ",".join([
		str(get_online_warriors(geolocs)),
		str(len(geolocs))
		])
	return ret_string


@run_scheduled
@app.route("/find-neighbors", methods=["GET"])
def finding_neighbors():
	global geolocs
	other_players = geolocs.copy()
	try:
		other_players.pop(request.remote_addr)
	except KeyError:
		pass
	return jsonify(other_players)


@run_scheduled
@app.route("/i-quit", methods=["POST"])
def quit():
	global geolocs
	del geolocs[str(request.remote_addr)]
	return ""


def set_home_base(init_geolocation):
	"""Set a home base upon the first entry to geolocs"""
	pickone = lambda : choice([True, False])

	new_coords = lambda dr, dist: uniform(0, (0.035 if dist else 0.95)) * (-1 if dr else 1)

	lat = new_coords(pickone(), pickone())
	lon = new_coords(pickone(), pickone())
	lat = init_geolocation['lat'] + lat
	lon = init_geolocation['lon'] + lon
	
	home_base['lat'] = lat
	home_base['lon'] = lon
	
	# print("Setting home_base @ {}".format(home_base))

	return 0