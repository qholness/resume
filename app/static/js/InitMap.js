function InitMap() {
    var map = L.map("Map").setView([41.9231, -87.7093], 10);

    // Map setup
    L.tileLayer('https://api.tiles.mapbox.com/v4/{id}/{z}/{x}/{y}.png?access_token={accessToken}', {
        maxZoom: 20,
        // zoomControl: false,
        continuousWorld: false,
        id: 'qholness.12608ddf',
        attribution: 'Map data &copy; <a href="http://openstreetmap.org">OpenStreetMap</a> contributors, <a href="http://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, Imagery © <a href="http://mapbox.com">Mapbox</a>',
        accessToken: 'pk.eyJ1IjoicWhvbG5lc3MiLCJhIjoiY2pnZTdyMzlzMDA2dTMzdDhwOTVtOTYxbyJ9.-71Qcyv-7z9veeSCQ7VJlQ'
    })
    .addTo(map);
    return map;
}