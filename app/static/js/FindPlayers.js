function FindPlayers() {
    var FindOtherPlayers = new XMLHttpRequest();
    FindOtherPlayers.onreadystatechange = function() {
        if (FindOtherPlayers.readyState == 4 && 
            FindOtherPlayers.status == 200) {
            var data = JSON.parse(FindOtherPlayers.responseText);
            for (x in data) {
                var loc = data[x];
                var coords = [loc['lat'], loc['lon']];
                var LocalIcon = Icons[loc['char']];
                LocalIcon = LocalIcon ? LocalIcon : RyuIcon;
                var is_online = loc['online'];
                var tag = loc['tag'];
                var found = false;
                for (layer in OtherPlayersGroup._layers) {
                    if (OtherPlayersGroup._layers[layer].options.id == x) {
                        OtherPlayersGroup._layers[layer].setIcon(LocalIcon);
                        OtherPlayersGroup._layers[layer].setLatLng(coords);
                        found = true;
                    }
                }
                if (found == false){
                    var newMarker;
                    newMarker = new L.marker(coords, {id : x});
                    newMarker.setIcon(LocalIcon);
                    newMarker.bindPopup(tag)
                    newMarker.addTo(OtherPlayersGroup);
                }
                if (is_online == "true") {
                    LocalIcon.options.shadowSize = [15, 15];
                } else {
                    LocalIcon.options.shadowSize = [0, 0];
                }
            }
        }
    }
    return FindOtherPlayers;
}