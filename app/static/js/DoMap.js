var map = InitMap();
map.zoomControl.setPosition('bottomright');
map.fitWorld();
map.flyTo([41.9231, -87.7093], 3.5);

$("#Map").css({
    position: 'absolute',
    zIndex: -10,
    height: '100%',
    width: '100%',
    top: '5%',
    left: 0
});

// Remove gray areas after CSS adjustments
map.invalidateSize();

var iconSelected = $("#VisIcon");
var taginput = $("#GamerTag");
var markerGroup = new L.layerGroup().addTo(map);
var OtherPlayersGroup = new L.layerGroup().addTo(map);
var lineGroup = new L.layerGroup().addTo(map);
var icon = iconSelected.val() ? Icons[iconSelected.val()] : RyuIcon;
var CoordinateManager = InitCoordinateManager();
var ThreeEsGroup = Place3sMarkers(map);
var FindPlayersService = FindPlayers();
var UnfollowService = InitUnfollowService();
var VisitorIcon = Icons[iconSelected.val()];
var VisitorLine = new L.polyline([]).addTo(lineGroup);
var VisitorMarker = new L.marker([0,0], {icon: icon});
// var HomeBaseService = InitHomeBase();
var UserPlaying = false;
var TrackVisitor = document.getElementById("TrackMe");
var ViewLock = document.getElementById("FindMe");
var online = document.getElementById("ImOnline");
var Zoom = document.getElementById("ZoomInt");
var TravelVec = [];
var hbset = false;
var salted = false;
var watchID;
var worker;

function StartGeoService() {
	VisitorLine.addTo(lineGroup);
	VisitorMarker.addTo(markerGroup);
	VisitorMarker.setPopupContent(Censor(taginput.val()));
	UserPlaying = true;
	watchID = navigator.geolocation.watchPosition(success, error, options);
}
$("#TrackMe").change(function() {
	if (TrackVisitor.checked || ViewLock.checked){
		if (ViewLock.checked) {
	        TrackVisitor.checked = true;
	    }
		if (UserPlaying == false) {
	        if(getCookie("tracked") == ""){
	        	setCookie("tracked", true, 365);
	        }
	        if(getCookie("online") == ""){
	        	if(online.checked){
	        		// setCookie("online", true, 365);
	        	}
	        }
			StartGeoService();
		}
	} else {
	    // Quit
	    UnfollowService.open("POST", "/i-quit");
	    UnfollowService.send();
	    UnfollowService.onreadystatechange = function(){
	    	if(UnfollowService.readyState == 4 && UnfollowService.status == 200){
			    navigator.geolocation.clearWatch(watchID);
			    markerGroup.removeLayer(VisitorMarker);
	    		UserPlaying = false;
	    		salted = false;
	    		setCookie("tracked", "", 0);
		    	alert("You have been removed from the map.");
	    	}
	    };
	}
});
if (getCookie("tracked") == "true") {
	TrackVisitor.checked = true;
	StartGeoService();
}
if (getCookie("online") == "true") {
	online.checked = true;
}
FindPlayersService.open("GET", "/find-neighbors");
FindPlayersService.send();
CoordinateManager.open("POST", "/coords");
CoordinateManager.send()