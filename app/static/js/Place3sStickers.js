var tempout;
function Place3sMarkers(){
	var group = L.layerGroup().addTo(map);
	var ThreeEsIcon = L.icon({
        iconUrl: '/static/images/3sIcon.png',
        iconSize: [42, 24]
    });
	$.getJSON("/static/data/3sLocations.json", function(data){
		var lat, lon, name, addy, url, coords;
		tempout = data;
		lats = Object.values(data["Latitude"]);
		lons = Object.values(data["Longitude"]);
		names = Object.values(data["Name"]);
		addys = Object.values(data["Full Address"]);
		urls = Object.values(data["Url"]);
		length = Object.size(data);
		lats.forEach(function(element, index) {
			if (element) {
				lat = lats[index];
				lon = lons[index];
				name = names[index];
				addy = addys[index]
				url = urls[index];
				coords = [lat, lon];
				L.marker(coords, {icon:ThreeEsIcon})
					.addTo(group)
					.bindPopup(name + "<br>" 
						+ addy 
						+ "<br><a target=_ href=" 
						+ url 
						+ ">" 
						+ url 
						+ "</a>");
			}
		});
	});
	return group;
}
