function coin_flip() {
    if (Math.random() < .5) {
        return 1;
    }
    return -1;
}