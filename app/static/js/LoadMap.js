var map = L.map("Map").fitWorld(),
	markerGroup = L.layerGroup().addTo(map),
	OtherPlayersGroup = L.layerGroup().addTo(map),
	lineGroup = L.layerGroup().addTo(map),
	VisitorLine = L.polyline([]).addTo(lineGroup),
	VisitorMarker = L.marker([0,0]).addTo(markerGroup),
	refreshCounter = 0,
	OtherPlayers = [],
	FollowMe = document.getElementById("FollowMe"),
	ViewLock = document.getElementById("ViewLock"),
	PlayerCount = document.getElementById("PlayerCount"),
	TravelVector = [],
	homebase_set = false,
	UserPlaying = false
	send_coordinates_to_server = new XMLHttpRequest(),
	homebase = new XMLHttpRequest(),
	UnFollowUser = new XMLHttpRequest(),
	FindOtherPlayers = new XMLHttpRequest(),
	GetCookieId = new XMLHttpRequest();

send_coordinates_to_server.onreadystatechange = function() {
	PlayerCount.innerHTML = send_coordinates_to_server.response;
}

homebase.onreadystatechange = function() {
	if (homebase.readyState == 4 && 
		homebase.status == 200) {
		coords = homebase.responseText.split(",");
		console.log("Setting Home Base at " + coords);
		L.marker(coords).addTo(markerGroup);
		homebase_set = true;
	}
}

FindOtherPlayers.onreadystatechange = function() {
	if (FindOtherPlayers.readyState == 4 && 
		FindOtherPlayers.status == 200) {

		data = JSON.parse(FindOtherPlayers.responseText);
		for (x in data) {
			loc = data[x];
			coords = [loc['lat'], loc['lon']];
			found = false;
			for (layer in OtherPlayersGroup._layers) {
				if (OtherPlayersGroup._layers[layer].options.id == x) {
				 OtherPlayersGroup._layers[layer].setLatLng(coords);
				 found = true;
				}
			}
			if (found == false) {
				var newMarker;
				newMarker = L.marker(coords, {id : x});
				newMarker.addTo(OtherPlayersGroup);
			}
		}
	}
}

// Map setup
L.tileLayer('https://api.tiles.mapbox.com/v4/{id}/{z}/{x}/{y}.png?access_token={accessToken}', {
    attribution: 'Map data &copy; <a href="http://openstreetmap.org">OpenStreetMap</a> contributors, <a href="http://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, Imagery © <a href="http://mapbox.com">Mapbox</a>',
    maxZoom: 20,
    continuousWorld: false,
    id: 'qholness.12608ddf',
    accessToken: 'pk.eyJ1IjoicWhvbG5lc3MiLCJhIjoiY2pnZTdyMzlzMDA2dTMzdDhwOTVtOTYxbyJ9.-71Qcyv-7z9veeSCQ7VJlQ'
})
.addTo(map);


// Geolocation success
function success(position){
	// Update geocoordinates
	var lat, lon;
	lat = position.coords.latitude;
	lon = position.coords.longitude;
	coords = [lat, lon];

	// Move home base when reached/alert players
	null;

	// Update coordinates
	if (FollowMe.checked) {
		// Set viewport
		map.locate({setView: true});
		UserPlaying = true;
		if (VisitorMarker.valueOf()._latlng != coords) {
			
			// Send user coordinates to server
			send_coordinates_to_server.open("POST", "/coords");
			send_coordinates_to_server.send(lat + "," + lon);
			
			// Push new coords to vector
			TravelVector.push(coords);

			// Update marker
			VisitorMarker.setLatLng(coords);

			// Update travel line.
			VisitorLine.setLatLngs(TravelVector);

			// Dock travelvector length
			if (TravelVector.length > 10000) {
				TravelVector.shift();
			}
		}
		// Update view
		if (ViewLock.checked) {
			map.setView(coords);
		}

		// Set homebase
		if (homebase_set == false) {
			homebase.open("GET", "/home-base");
			homebase.send();
		}

		FindOtherPlayers.open("GET", "/find-neighbors");
		FindOtherPlayers.send();

	} else {
		// 
		if (UserPlaying) {
			UnFollowUser.open("POST", "/i-quit");
			UnFollowUser.send();
			markerGroup.remove(VisitorMarker);
			UserPlaying = false;
		}
	}
}

// Geolocation fail
function error(err){
	console.warn('ERROR(' + err.code + '): ' + err.message);
}

// Configuration
options = {
	enableHighAccuracy: true,
	timeout: 8000,
	maximumAge: 0
};

watchID = navigator.geolocation.watchPosition(success, error, options);