function InitHomeBase() {
    homebase = new XMLHttpRequest();

    homebase.onreadystatechange = function() {
        if (homebase.readyStateyState == 4 && 
            homebase.status == 200) {
            coords = homebase.responseText.split(",");
            L.marker(coords).addTo(markerGroup);
            homebase_set = true;
        }
    }
    return homebase;
}