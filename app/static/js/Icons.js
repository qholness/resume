var iconSize = [48, 48];
var popupAnchor = [-3, -20];
var shadowStandardSize = [0, 0];
var shadowStandardAnchor = [9, 42];

var AlexIcon = L.icon({
    iconUrl: 'static/images/alex.gif',
    iconSize: iconSize,
    popupAnchor: popupAnchor,
    shadowUrl: 'static/images/exclaim.png',
    shadowSize: shadowStandardSize,
    shadowAnchor: shadowStandardAnchor
});
var AkumaIcon = L.icon({
    iconUrl: 'static/images/akuma.gif',
    iconSize: iconSize,
    popupAnchor: popupAnchor,
    shadowUrl: 'static/images/exclaim.png',
    shadowSize: shadowStandardSize,
    shadowAnchor: shadowStandardAnchor
});
var ChunIcon = L.icon({
    iconUrl: 'static/images/chun.gif',
    iconSize: iconSize,
    popupAnchor: popupAnchor,
    shadowUrl: 'static/images/exclaim.png',
    shadowSize: shadowStandardSize,
    shadowAnchor: shadowStandardAnchor
});
var DudleyIcon = L.icon({
    iconUrl: 'static/images/dudley.gif',
    iconSize: iconSize,
    popupAnchor: popupAnchor,
    shadowUrl: 'static/images/exclaim.png',
    shadowSize: shadowStandardSize,
    shadowAnchor: shadowStandardAnchor
});
var ElenaIcon = L.icon({
    iconUrl: 'static/images/elena.gif',
    iconSize: iconSize,
    popupAnchor: popupAnchor,
    shadowUrl: 'static/images/exclaim.png',
    shadowSize: shadowStandardSize,
    shadowAnchor: shadowStandardAnchor
});
var HugoIcon = L.icon({
    iconUrl: 'static/images/hugo.gif',
    iconSize: iconSize,
    popupAnchor: popupAnchor,
    shadowUrl: 'static/images/exclaim.png',
    shadowSize: shadowStandardSize,
    shadowAnchor: shadowStandardAnchor
});
var IbukiIcon = L.icon({
    iconUrl: 'static/images/ibuki.gif',
    iconSize: iconSize,
    popupAnchor: popupAnchor,
    shadowUrl: 'static/images/exclaim.png',
    shadowSize: shadowStandardSize,
    shadowAnchor: shadowStandardAnchor
});
var KenIcon = L.icon({
    iconUrl: 'static/images/ken.gif',
    iconSize: iconSize,
    popupAnchor: popupAnchor,
    shadowUrl: 'static/images/exclaim.png',
    shadowSize: shadowStandardSize,
    shadowAnchor: shadowStandardAnchor
});
var MakotoIcon = L.icon({
    iconUrl: 'static/images/makoto.gif',
    iconSize: iconSize,
    popupAnchor: popupAnchor,
    shadowUrl: 'static/images/exclaim.png',
    shadowSize: shadowStandardSize,
    shadowAnchor: shadowStandardAnchor
});
var NecroIcon = L.icon({
    iconUrl: 'static/images/necro.gif',
    iconSize: iconSize,
    popupAnchor: popupAnchor,
    shadowUrl: 'static/images/exclaim.png',
    shadowSize: shadowStandardSize,
    shadowAnchor: shadowStandardAnchor
});
var OroIcon = L.icon({
    iconUrl: 'static/images/oro.gif',
    iconSize: iconSize,
    popupAnchor: popupAnchor,
    shadowUrl: 'static/images/exclaim.png',
    shadowSize: shadowStandardSize,
    shadowAnchor: shadowStandardAnchor
});
var QIcon = L.icon({
    iconUrl: 'static/images/q.gif',
    iconSize: iconSize,
    popupAnchor: popupAnchor,
    shadowUrl: 'static/images/exclaim.png',
    shadowSize: shadowStandardSize,
    shadowAnchor: shadowStandardAnchor
});
var RemyIcon = L.icon({
    iconUrl: 'static/images/remy.gif',
    iconSize: iconSize,
    popupAnchor: popupAnchor,
    shadowUrl: 'static/images/exclaim.png',
    shadowSize: shadowStandardSize,
    shadowAnchor: shadowStandardAnchor
});
var RyuIcon = L.icon({
    iconUrl: 'static/images/ryu.gif',
    iconSize: iconSize,
    popupAnchor: popupAnchor,
    shadowUrl: 'static/images/exclaim.png',
    shadowSize: shadowStandardSize,
    shadowAnchor: shadowStandardAnchor
});
var SeanIcon = L.icon({
    iconUrl: 'static/images/sean.gif',
    iconSize: iconSize,
    popupAnchor: popupAnchor,
    shadowUrl: 'static/images/exclaim.png',
    shadowSize: shadowStandardSize,
    shadowAnchor: shadowStandardAnchor
});
var TwelveIcon = L.icon({
    iconUrl: 'static/images/twelve.gif',
    iconSize: iconSize,
    popupAnchor: popupAnchor,
    shadowUrl: 'static/images/exclaim.png',
    shadowSize: shadowStandardSize,
    shadowAnchor: shadowStandardAnchor
});
var UrienIcon = L.icon({
    iconUrl: 'static/images/urien.gif',
    iconSize: iconSize,
    popupAnchor: popupAnchor,
    shadowUrl: 'static/images/exclaim.png',
    shadowSize: shadowStandardSize,
    shadowAnchor: shadowStandardAnchor
});
var YangIcon = L.icon({
    iconUrl: 'static/images/yang.gif',
    iconSize: iconSize,
    popupAnchor: popupAnchor,
    shadowUrl: 'static/images/exclaim.png',
    shadowSize: shadowStandardSize,
    shadowAnchor: shadowStandardAnchor
});
var YunIcon = L.icon({
    iconUrl: 'static/images/yun.gif',
    iconSize: iconSize,
    popupAnchor: popupAnchor,
    shadowUrl: 'static/images/exclaim.png',
    shadowSize: shadowStandardSize,
    shadowAnchor: shadowStandardAnchor
});

var ExclaimIcon = L.icon({
    iconUrl: 'static/images/exclaim.png',
    iconSize: [20, 20],
    iconAncher: [0, 10]
});

var Icons = {
    "Alex" : AlexIcon,
    "Akuma" : AkumaIcon,
    "Chun Li" : ChunIcon,
    "Dudley" : DudleyIcon,
    "Elena" : ElenaIcon,
    "Hugo" : HugoIcon,
    "Ibuki" : IbukiIcon,
    "Ken" : KenIcon,
    "Makoto" : MakotoIcon,
    "Necro" : NecroIcon,
    "Oro" : OroIcon,
    "Q" : QIcon,
    "Remy" : RemyIcon,
    "Ryu" : RyuIcon,
    "Sean" : SeanIcon,
    "Twelve" : TwelveIcon,
    "Urien" : UrienIcon,
    "Yang" : YangIcon,
    "Yun" : YunIcon
};