function InitCoordinateManager() {
    manager = new XMLHttpRequest();
    manager.onreadystatechange = function() {
    	var data = manager.response.split(",");
    	var online = data[0];
    	var total = data[1];
        if ($("#OnlineCount").html() != online){
        	$("#OnlineCount").html(online)
        };
        if ($("#PlayerCount").html() != total) {
        	$("#PlayerCount").html(total)
        };
    }
    return manager;
}