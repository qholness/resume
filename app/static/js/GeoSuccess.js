function success(position) {
    // Update geocoordinates
    // var lat, lon;
    if (salted == false) {
        lat_salt = salt_geodata();
        lon_salt = salt_geodata();
        salted = true;
    }
    lat =  position.coords.latitude + lat_salt;
    lon = position.coords.longitude + lon_salt;
    coords = [lat, lon];
    
    // Update coordinates
    if (VisitorMarker.valueOf()._latlng != coords) {
        var censoredTag = Censor(taginput.val());
        // Send user coordinates to server
        CoordinateManager.open("POST", "/coords");
        CoordinateManager.send(
            lat
            + ","
            + lon
            + ","
            + iconSelected.val()
            + ","
            + censoredTag
            + ","
            + online.checked
        );
        
        // Push new coords to vector
        // TravelVec.push(coords);

        // Update marker
        VisitorMarker.setLatLng(coords);

        // Update travel line.
        // VisitorLine.setLatLngs(TravelVec);

        // Dock Travel vector length
        // if (TravelVec.length > 100000) {
        //     TravelVec.shift();
        // }
    }

    // Update view
    if (ViewLock.checked == true) {
        map.setView(coords);
    }

    // Update popup
    VisitorMarker.setPopupContent(censoredTag);

    // Set homebase
    // if (hbset == false) {
        // HomeBaseService.open("GET", "/home-base");
        // HomeBaseService.send();
        // hbset = true;
    // }
    }


// Geolocation fail
function error(err){
    console.warn('ERROR(' + err.code + '): ' + err.message);
}