function RemoveSpecialCharacters(tag) {
	specChars = "~`!@#$%^&*)(+_-=][}{\\|'\";:/?.>,<".split("")
	specChars.forEach( function(element, index) {
		tag = tag.replace(element, ".");
	});
	return tag;
}

function Truncate(tag) {
	return RemoveSpecialCharacters(tag.slice(0, 32));
}

function Censor(tag) {
	var string = Truncate(tag.toLowerCase());
	var bad_words = ["ass", "fuck", "shit", "nigga", "nigger", "bitch", 
		"tit", "boob", "anus", "anal", "$h1t", "sh1t", "$hit",
		"kike", "nazi", "spick", "cunt", "penis", "dick", "p3nis",
		"p3ni\$", "a$s", "a$\$", "as\$"]
	bad_words.forEach( function(element, index) {
		string = string.replace(element, "****");
	});
	return string;
}