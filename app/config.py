class Config(object):
    DEBUG=False
    TESTING=False
    DATABASE_URI = ''
    SECRET_KEY = 'dev'


class ProdConfig(Config):
    from uuid import uuid4
    SECRET_KEY = str(uuid4())


class DevConfig(Config):
	print("#### Starting server with Dev config ####")
	from uuid import uuid4
	SECRET_KEY = str(uuid4())
	DEBUG=True


class TestConfig(Config):
    TESTING=True